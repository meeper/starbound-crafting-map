/**
 * The JS page for the crafting tree.
 */

/**
 * Global variables
 */
var recipeLoadStatus = 'loading';
var recipes = {};
var itemNameLoadStatus = 'loading';
var itemNames = {};
var itemDetails = {};
var currentSearchItemKey = null;
var itemNodeDisplayTemplate = null;

/**
 * This function gets the page going once it has been fully loaded from the
 * server.
 */
$(document).ready(function() {
	$.ajaxSetup({
		'dataType' : 'json',
		'type' : 'GET',
		'timeout' : 30000
	});
	itemNodeDisplayTemplate = $.templates('#itemNodeDisplayTemplate');
	$('#searchTabBox, #newsTabBox').tabs();
	$('#resultsTabBox').tabs({
		activate: function (event, ui) {
			//setup all things that need to be done when a tab is selected
			if (currentSearchItemKey != null) {
				checkItemLoadState();
			}
		}
	});
	$('#clearItemButton').button().click(function(event) {
		$("#itemLocatorSelecter").val('').focus();
	});
	$('#resultsTabBox div.treeOuterDisplayDiv').on('click', 'a.switchToItemLink', function() {
		newSearchItemSelected($(this).attr('data-item-key'));
		$("#itemLocatorSelecter").val('');
	});
	
	$("div.tabMsgDiv").empty().show().text("Loading... ").append($('<img/>').attr('src','common/icon_thumper.gif'));
	$("#craftingItemUrlDiv").hide();
	loadRecipes();
	loadItemNames();
}); //end document ready

function loadItemNames() {
	$.ajax({
		'url' : jsgDataDir + 'itemNames.json',
		'data': {},
		'error': function () {
		}, //end error function
		'complete': function() {
			itemNameLoadStatus = 'done';
			checkInitialLoading();
		}, //end complete function
		'success': function (jsonObj) {
			itemNames = jsonObj;
			$("#itemLocatorSelecter").easyAutocomplete({
				'data': itemNames,
				'placeholder': "Start typing item name here",
				'getValue': 'name',
				'list': {
					'maxNumberOfElements': 20,
					'match': {
						'enabled': true
					},
					'onChooseEvent': newSearchItemSelected
				}
			});
		} //end success function
	}); //end ajax call
}

function loadRecipes() {
	$.ajax({
		'url' : jsgDataDir + 'recipes.json',
		'data': {},
		'error': function () {
		}, //end error function
		'complete': function() {
			recipeLoadStatus = 'done';
			checkInitialLoading();
		}, //end complete function
		'success': function (jsonObj) {
			recipes = jsonObj;
		} //end success function
	}); //end ajax call
}

function checkInitialLoading() {
	if (recipeLoadStatus == 'done' && itemNameLoadStatus == 'done') {
		$('#searchTabMsgDiv').hide();
		$('div.resultTabMsgDiv').empty().show().text("Waiting for item selection.");
		
		//load initial item from GET parameter
		var pageAddress = window.location.search; // Address of the current window
		var parameterList = new URLSearchParams(pageAddress); // Returns a URLSearchParams object instance
		var initialItem = parameterList.get("item"); //gets the value set by the items name
		if (initialItem != null || initialItem != "") {
			newSearchItemSelected(initialItem);
		}
	}
}

function newSearchItemSelected(specificItem) {
	if (specificItem == undefined || specificItem == null || specificItem == "") {
		currentSearchItemKey = $("#itemLocatorSelecter").getSelectedItemData().key;
	}
	else {
		currentSearchItemKey = specificItem;
	}
	
	//check for valid item selection
	var found = false;
	for (var index in itemNames) {
		if (currentSearchItemKey == itemNames[index].key) {
			found = true;
			break;
		}
	}
	if (!found) {
		return;
	}
	
	//set messages
	$('div.resultTabMsgDiv').empty().show().text("Loading Item details... ").append($('<img/>').attr('src','common/icon_thumper.gif'));
	$('#craftingItemUrlDiv').show();
	var itemLink = window.location.href.split("?")[0] + "?item=" + currentSearchItemKey;
	$('#itemFullURLLink').empty().prop('href', itemLink).text(itemLink);
	
	//find all items that we need to retrieve
	findItemBuiltFrom(currentSearchItemKey);
	findItemUsedIn(currentSearchItemKey);
	
	//initiate retrieval of items
	var alreadyLoaded = true;
	for (var itemKey in itemDetails) {
		if (itemDetails[itemKey] == 'loading') {
			alreadyLoaded = false;
			$.ajax({
				'url' : jsgDataDir + 'items/' + itemKey + '.json',
				'data': {},
				'error': function () {
				}, //end error function
				'complete': function() {
				}, //end complete function
				'success': function (jsonObj) {
					itemDetails[jsonObj.itemKey] = jsonObj;
					checkItemLoadState();
				} //end success function
			}); //end ajax call
		}
	}
	if (alreadyLoaded = true) {
		checkItemLoadState();
	}
}

function findItemBuiltFrom(itemKey) {
	//get base item details
	if (itemDetails[itemKey] == undefined) {
		itemDetails[itemKey] = 'loading';
	}
	
	//get item inputs
	var recipeObj = recipes[itemKey];
	if (recipeObj == undefined || recipeObj.input == undefined) {
		return;
	}
	for (var currInputKey in recipeObj.input) {
		var currInputItem = recipeObj.input[currInputKey].item;
		findItemBuiltFrom(currInputItem);
	}
}

function findItemUsedIn(itemKey) {
	//get base item details
	if (itemDetails[itemKey] == undefined) {
		itemDetails[itemKey] = 'loading';
	}
	
	//get item outputs
	for (var recipeKey in recipes) {
		if (!recipes.hasOwnProperty(recipeKey)) continue;
		var currRecipeObj = recipes[recipeKey];
		if (currRecipeObj.input == undefined) {
			return;
		}
		for (var currInput in currRecipeObj.input) {
			var currRecipeInputItemObj = currRecipeObj.input[currInput];
			if (currRecipeInputItemObj.item == itemKey) {
				findItemUsedIn(recipeKey);
			}
		}
	}
}

function checkItemLoadState() {
	//check if all items have been loaded
	for (var itemKey in itemDetails) {
		if (itemDetails[itemKey] == 'loading') {
			return;
		}
	}
	//if we reach here, we're all loaded
	updateItemDisplay();
}

function updateItemDisplay() {
	$('#createdFromTabMsgDiv, #createdWithTabMsgDiv').hide();
	$('#craftingItemDisplayDiv').empty();
	$('div.resultTabMsgDiv').empty().hide();
	var activeTab = $('#resultsTabBox').tabs("option","active");  //returns index number of tab
	var itemKey = currentSearchItemKey; //returns the 'key' value of the selected object
	var myRecipe = recipes[itemKey];
	
	if (activeTab == 0) {
		//created with tab
		var inputItemObjects = drawItemBuiltFrom(itemKey);
		$('#craftingTreeItemInput').empty().height('400px');
		var itemInputChart = new Treant({
			'chart' : {
				'container' : '#craftingTreeItemInput',
				'rootOrientation' : 'EAST',
				'connectors' : {
					'type' : 'bCurve'
				},
				'node' : {
					'HTMLclass' : 'treeNode'
				}
			},
			'nodeStructure' : inputItemObjects
		}, function() {
			//var treeHeight = $('#craftingTreeItemInput svg').attr('height');
			//$('#craftingTreeItemInput').height(treeHeight + "px");
			updateViewingHeight('#craftingTreeItemInput');
		});	
	
	}
	else if (activeTab == 1) {
		//created from tab
		var outputItemObjects = drawItemUsedIn(itemKey);
		outputItemObjects['collapsed'] = false;
		$('#craftingTreeItemOutput').empty().height('400px');
		var itemOutputChart = new Treant({
			'chart' : {
				'container' : "#craftingTreeItemOutput",
				'rootOrientation' : 'WEST',
				'levelSeparation' : 50,
				'connectors' : {
					'type' : 'step'
				},
				'node' : {
					'HTMLclass' : 'treeNode',
					'collapsable' : true
				}
			},
			'nodeStructure' : outputItemObjects
		},function() {
			//var treeHeight = $('#craftingTreeItemOutput svg').attr('height');
			//$('#craftingTreeItemOutput').height(treeHeight + "px");
			updateViewingHeight('#craftingTreeItemOutput');
			
			$('.Treant .collapse-switch').empty().text("[+]");
			$('#craftingTreeItemOutput').on('click',function() {
				updateViewingHeight('#craftingTreeItemOutput');
			});
		});
	}	
}

function updateViewingHeight(containerSelector) {
	var treeHeight = $(containerSelector).find('svg').attr('height');
	$(containerSelector).height(treeHeight + "px");
}


function drawItemBuiltFrom(itemKey, itemQtyNeeded) {
	var itemDetail = itemDetails[itemKey];
	if (itemDetail == undefined) {
		//alert(itemKey + " item details are missing.");
		return null;
	}
	itemDetail = $.extend({},itemDetails[itemKey]);

	if (itemQtyNeeded != undefined && itemQtyNeeded != null) {
		itemDetail['quantityNeeded'] = itemQtyNeeded;
	}

	var recipeObj = recipes[itemKey];
	if (recipeObj != undefined && recipeObj.output != undefined) {
		itemDetail['quantityProduced'] = recipeObj.output.count;
		itemDetail['gamePath'] = recipeObj.gameLocation;
	}
	var newNode = drawItemTreeDetailNode(itemDetail);
	if (recipeObj != undefined && recipeObj.input != undefined) {
		
		for (var currInputKey in recipeObj.input) {
			var currInputItem = recipeObj.input[currInputKey].item;
			var currInputQtyNeeded = recipeObj.input[currInputKey].count;
			var childInput = drawItemBuiltFrom(currInputItem, currInputQtyNeeded);
			newNode.children.push(childInput);
		}
	}
	return newNode;
}

function drawItemUsedIn(itemKey) {
	var itemDetail = itemDetails[itemKey];
	if (itemDetail == undefined) {
		//alert(itemKey + " item details are missing.");
		return null;
	}
	var newNode = drawItemTreeDetailNode(itemDetail);
	for (var recipeKey in recipes) {
		if (!recipes.hasOwnProperty(recipeKey)) continue;
		var currRecipeObj = recipes[recipeKey];
		if (currRecipeObj.input != undefined) {
			for (var currInput in currRecipeObj.input) {
				var currRecipeInputItemObj = currRecipeObj.input[currInput];
				if (currRecipeInputItemObj.item == itemKey) {
					var itemUsedIn = drawItemUsedIn(recipeKey);
					newNode.children.push(itemUsedIn);
				}
			}
		}
	}
	if (newNode.children.length > 5) {
		newNode['collapsed'] = true;
	}
	return newNode;
}


function drawItemTreeDetailNode(itemDetailObj) {
	var nodeHTML = itemNodeDisplayTemplate.render(itemDetailObj, {
		'wikiLink' : function (itemName) {
			return starbounderWikiLinkPrefix + itemName.replace(/ /g,"_");
		} //end function wikiLink
	});
	return {
		'innerHTML' : nodeHTML,
		'children' : []
	};
}


