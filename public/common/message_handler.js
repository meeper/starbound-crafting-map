/**
 * This is the main object used to handle both the error and notice messages
 * 
 * @param string inputType Can be either one of either 'error' or 'notice'.
 */
function msgHandler (inputType) {
	var currentlyScrolling = false;
	var msgContainerId;
	var msgTemplateText;
	
	if (inputType == 'error') {
		msgContainerId = "#jsglobalErrorDisplayArea";
		msgTemplateText = 
			'<div class="ui-state-default ui-state-error ui-corner-all" style="padding: 0pt 0.7em; display: none">' +
				'<p style="font-size: 1.1em">' +
					'<span class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.3em"></span>' +
					'<strong>Error:</strong> {{>msg}}' +
				'</p>' +
			'</div>';
	} else if (inputType == 'notice') {
		msgContainerId = "#jsglobalNoticeDisplayArea";
		msgTemplateText = 
			'<div class="ui-state-default ui-state-highlight ui-corner-all" style="padding: 0pt 0.7em; display: none">' +
				'<p style="font-size: 1.1em">' +
					'<span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em"></span>' +
					'<strong>Notice:</strong> {{>msg}}' +
				'</p>' +
			'</div>';
	} else {
		throw ('Invalid message type call.');
	} // end if inputType

	/**
	 * This method is used to add a message to the display.
	 */
	this.add = function (messageText) {
		//add message to error box
		var messageData = {
			"msg" : messageText
		};
		
		var msgTemplate = $.templates(msgTemplateText);
		var messageHtml = msgTemplate.render(messageData);
		$(msgContainerId).append(messageHtml);
		
		//scroll the browser to the top (if needed) so the user sees the message
		if ($(window).scrollTop() > 50 && !currentlyScrolling) {
			currentlyScrolling = true;
			$('html').animate({scrollTop: 0}, 500, function() {
				currentlyScrolling = false;
				});
		}
		
		//roll the message onto the screen
		var showList = $(msgContainerId).find('div');
		showList.show('blind', {}, 500);
	}; // end add method

	/**
	 * This method is used to clear the message display area
	 */
	this.clear = function() {
		var clearingList = $(msgContainerId).find('div');
		clearingList.hide('blind', {}, 500, function() {
			$(this).remove();
		});
	}; // end clear method
	
	/**
	 * This method is used to clean the JSON response object and provide either
	 * null if no messages of this type exist or a guaranteed array of one or 
	 * more.  This is to account for the possibility of no message, a single 
	 * json object for a message, or multiple messages without having a lot of
	 * check code sprinkled everywhere.
	 * @param The raw response object from the AJAX call.
	 */
	this.clean = function(responseObj) {
		if (!responseObj) {
			//there is no response object
			return null;
		}
		if (!responseObj[inputType]) {
			//there are no messages of the given type
			return null;
		}
		if (!$.isArray(responseObj[inputType])) {
			return [responseObj[inputType]];
		}
		return responseObj[inputType];
	}; //end clean method

} // end msg_handler

var errors = new msgHandler('error');
var notices = new msgHandler('notice');
