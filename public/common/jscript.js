var appUrl = "https://meeper.gitlab.io/starbound-crafting-map/";
var contactUrl = "https://gitlab.com/meeper/starbound-crafting-map/-/issues";
var jsgDataDir = "data/"
var starbounderWikiLinkPrefix = "https://starbounder.org/";

$(document).ajaxError(function(eventObj, xmlHttpRequestObj, ajaxOptions, thrownError) {
	var error_text = 'AJAX Error.  ';
	if (eventObj.type == 'ajaxError') {
		error_text += 'Server Error: ' + xmlHttpRequestObj.status + ' ' + xmlHttpRequestObj.statusText;
	} else if (thrownError == 'timeout') {
		error_text += 'Server Timeout.';
	} else if ((thrownError == 'parsererror') || (thrownError.substring(0,12) == 'Invalid JSON')) {
		error_text += 'Parse Error.';
	} else {
		error_text += 'Unknown Error.';
	}
	errors.add(error_text);
}); //end ajaxError
		
$(window).bind('beforeunload', function() {
	$(document).unbind('ajaxError');
}); //end bind beforeunload

$(document).ready(function() {
	$.ajaxSetup({
		'dataType' : 'json',
		'type' : 'GET',
		'timeout' : 30000
	});
	
	$('div.tabBox').tabs();
	$('a.appUrlLink').attr('href',appUrl).text(appUrl);
	$('a.contactLink').attr('href',contactUrl);
	
	//add common nav menu
	$('#pageHeaderNavLargeDiv')
		.append($('<a href="index.html">Home</a>'))
		.append($('<a href="crafting_map.html">Crafting Map</a>'))
		.append($('<a href="crafting_ability_matrix.html">Crafting Matrix</a>'))
		.append($('<a href="' + contactUrl + '">Contact</a>'))
		.controlgroup();
	
	//add common footer
	$('#pageFooterDiv').find("div")
		.append('<a href="termsandconditions.html">Terms and Conditions</a>')
		.append(' | ')
		.append('<a href="privacypolicy.html">Privacy Policy</a>')
		.append(' | ')
		.append('<a href="cookiepolicy.html">Cookie Policy</a>');
}); //end document ready
