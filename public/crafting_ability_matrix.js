/**
 * The JS page for the crafting ability matrix.
 */

/**
 * Global variables
 */
var recipeLoadStatus = 'loading';
var recipes = {};
var itemNameLoadStatus = 'loading';
var itemNames = {};
var itemDetails = {};
var currentSearchItemKey = null;
var inventoryItemEntryTemplate = null;
var creatableItemEntryTemplate = null;

/**
 * This function gets the page going once it has been fully loaded from the
 * server.
 */
$(document).ready(function() {
	//setup
	inventoryItemEntryTemplate = $.templates('#inventoryItemEntryTemplate');
	creatableItemEntryTemplate = $.templates('#creatableItemEntryTemplate');
	$('#searchTabBox, #resultsTabBox').tabs();
	$("#inventoryItemsUrlDiv, #inventoryTable, #creatableItemTable").hide();

	//attach handlers
	$('#getPossibilitiesButton').button().click(function(event) {
		determinePossibilities();
	}).hide();
	$('#inventoryTableBody')
		.on('click','img.inventoryIconPause', function() {
			var row = $(this).parents('tr.inventoryItemRow');
			row.attr('data-disabled', 'true');
			row.find('img.inventoryIconPause').hide();
			row.find('img.inventoryIconPlay').show();
			row.addClass('disabledItem');
		})
		.on('click','img.inventoryIconPlay', function() {
			var row = $(this).parents('tr.inventoryItemRow');
			row.attr('data-disabled', 'false');
			row.find('img.inventoryIconPlay').hide();
			row.find('img.inventoryIconPause').show();
			row.removeClass('disabledItem');
		})
		.on('click','img.inventoryIconDelete', function() {
			$(this).parents('tr.inventoryItemRow').remove();
			updateInventoryUrl();
		});
	
	
	//trigger startup
	$("div.tabMsgDiv").empty().show().text("Loading... ").append($('<img/>').attr('src','common/icon_thumper.gif'));
	loadRecipes();
	loadItemNames();
}); //end document ready

function getItemDetails(itemKey, callbackFunc) {
	if (itemDetails[itemKey] == undefined) {
		$.ajax({
			'url' : jsgDataDir + 'items/' + itemKey + '.json',
			'success': function (jsonObj) {
				itemDetails[jsonObj.itemKey] = jsonObj;
				callbackFunc(itemKey);
			} //end success function
		}); //end ajax call
	}
	else {
		callbackFunc(itemKey);
	}
}

function loadItemNames() {
	$.ajax({
		'url' : jsgDataDir + 'itemNames.json',
		'complete': function() {
			itemNameLoadStatus = 'done';
			checkInitialLoading();
		}, //end complete function
		'success': function (jsonObj) {
			itemNames = jsonObj;
			$("#itemLocatorSelecter").easyAutocomplete({
				'data': itemNames,
				'placeholder': "Start typing item name here",
				'getValue': 'name',
				'list': {
					'maxNumberOfElements': 20,
					'match': {
						'enabled': true
					},
					'onChooseEvent': newSearchItemSelected
				}
			});
		} //end success function
	}); //end ajax call
}

function loadRecipes() {
	$.ajax({
		'url' : jsgDataDir + 'recipes.json',
		'complete': function() {
			recipeLoadStatus = 'done';
			checkInitialLoading();
		}, //end complete function
		'success': function (jsonObj) {
			recipes = jsonObj;
		} //end success function
	}); //end ajax call
}

function checkInitialLoading() {
	if (recipeLoadStatus == 'done' && itemNameLoadStatus == 'done') {
		$('#searchTabMsgDiv, #createdWithTabMsgDiv').empty().hide();

		//load starter list of inventory items
		var pageAddress = window.location.search; // Address of the current window
		var parameterList = new URLSearchParams(pageAddress); // Returns a URLSearchParams object instance
		var initialItemList = parameterList.get("items"); //gets the value set by the items name
		if (initialItemList != null) {
			var initialItemKeys = initialItemList.split(',');
			for (var index in initialItemKeys) {
				newSearchItemSelected(initialItemKeys[index]);
			}
		}
	}
}

function newSearchItemSelected(specificItem) {
	if (specificItem == undefined || specificItem == null || specificItem == "") {
		currentSearchItemKey = $("#itemLocatorSelecter").getSelectedItemData().key;
	}
	else {
		currentSearchItemKey = specificItem;
	}	
	//check for valid item selection
	var found = false;
	for (var index in itemNames) {
		if (currentSearchItemKey == itemNames[index].key) {
			found = true;
			break;
		}
	}
	if (!found) {
		return;
	}
	
	//add item to table
	$('#searchTabMsgDiv').empty().show().text("Loading Item details... ").append($('<img/>').attr('src','common/icon_thumper.gif'));
	var inventoryTableBody = $('#inventoryTableBody');
	$('#inventoryTable, #getPossibilitiesButton').show();
	getItemDetails(currentSearchItemKey, function(itemKey) {
		inventoryTableBody.append(inventoryItemEntryTemplate.render(itemDetails[itemKey], {
			'wikiLink' : function (itemName) {
				return starbounderWikiLinkPrefix + itemName.replace(/ /g,"_");
			} //end function wikiLink
		}));
		$('#searchTabMsgDiv').empty().hide();
		
		//sort table and update url
		sortInventoryTable();
	}); //end getItemDetails
	
	$("#itemLocatorSelecter").val('').focus();
}

function determinePossibilities() {
	$('#createableItemTableBody').empty();
	$('#creatableItemTable').show();
	
	//get current inventory
	var inventoryItemKeys = $("#inventoryTableBody tr[data-disabled!='true']").map(function() {
		return $(this).attr('data-item-key');
	}).get();
	
	//run through recipies looking for matches to inventory
	var createableItemTableBody = $("#createableItemTableBody");
	var possibleItemKeys = [];
	var newItemsFound = true;
	while (newItemsFound) {
		newItemsFound = false;
		$.each(recipes, function(recipeItemKey, recipeItemData) {
			if ($.inArray(recipeItemKey, possibleItemKeys) > -1) {
				//we've already added the current item to the list of possible items to use as ingredients, skip it.
				return;
			}
			var possible = true;
			for (var index in recipeItemData.input) {
				if ($.inArray(recipeItemData.input[index].item, inventoryItemKeys) == -1 && $.inArray(recipeItemData.input[index].item, possibleItemKeys) == -1) {
					//ingredients list is not in the given inventory or possible inventory lists
					possible = false;
					break;
				}
			}
			if (possible) {
				//add to the list of possible items
				possibleItemKeys.push(recipeItemKey);
				newItemsFound = true;
				
				//draw table row with option
				getItemDetails(recipeItemKey, function(itemKey) {
					var itemObj = itemDetails[itemKey];
					var inputList = recipes[itemKey].input;
					
					//get dependancy information
					itemObj['depDetails'] = [];
					itemObj['depTotal'] = 0;
					var entryNum = 0;
					for (var i in inputList) {
						itemObj['depDetails'].push({
							'index' : entryNum,
							'name'  : itemDetails[inputList[i].item].itemName,
							'qty'   : inputList[i].count,
							'price' : itemDetails[inputList[i].item].price
						});
						itemObj['depTotal'] += itemDetails[inputList[i].item].price * inputList[i].count;
						entryNum += 1;
					}
					
					//draw
					var row = creatableItemEntryTemplate.render(itemObj, {
						'wikiLink' : function (itemName) {
							return starbounderWikiLinkPrefix + itemName.replace(/ /g,"_");
						} //end function wikiLink
					});
					createableItemTableBody.append(row);
					sortResultTable();
				});
			}
		});
	} //end while newItemsFound
} //end function determinePossibilities

function sortInventoryTable() {
	var rows = $('#inventoryTableBody tr').get();
	rows.sort(function(a,b) {
		var aText = $(a).children('td').eq(0).text().toUpperCase();
		var bText = $(b).children('td').eq(0).text().toUpperCase();
		if (aText > bText) {
			return 1;
		}
		else if (aText < bText) {
			return -1;
		}
		else {
			return 0;
		}
	}) //end rows.sort
	$.each(rows, function(index, row) {
		$('#inventoryTableBody').append(row);
	});
	updateInventoryUrl();
}

function updateInventoryUrl() {
	var inventoryItemKeys = $("#inventoryTableBody tr").map(function() {
		return $(this).attr('data-item-key');
	}).get();
	$('#inventoryItemsUrlDiv').show();
	var itemsLink = window.location.href.split("?")[0] + "?items=" + inventoryItemKeys;
	var itemsLinkText = itemsLink.length > 100 ? itemsLink.substring(0,100) + "..." : itemsLink;
	$('#inventoryItemsFullURLLink').empty().prop('href', itemsLink).text(itemsLinkText);

}

function sortResultTable() {
	var rows = $('#createableItemTableBody tr').get();
	rows.sort(function(a,b) {
		var aText = $(a).children('td').eq(4).text().toUpperCase();
		var bText = $(b).children('td').eq(4).text().toUpperCase();
		if (bText - aText == 0) {
			var aText = $(a).children('td').eq(5).text().toUpperCase();
			var bText = $(b).children('td').eq(5).text().toUpperCase();
		}
		if (bText - aText == 0) {
			var aText = $(a).children('td').eq(0).text().toUpperCase();
			var bText = $(b).children('td').eq(0).text().toUpperCase();
		}
		return bText - aText;
	}); //end rows.sort
	$.each(rows, function(index, row) {
		$('#createableItemTableBody').append(row);
	});
}

