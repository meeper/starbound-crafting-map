# Starbound Crafting Map

Two crafting assistance pages that can be used by users of the game Starbound.

The first page gives you a map of what is needed to make and item and what an item can be used to create.

The second page gives you a list of food items that can be created based on what you currently have available.

## Getting started

This application is a basic HTML/JS application with no server side processing required beyond serving static files.

The files in this application's data directory should come from the [Starbound Asset Converter](https://gitlab.com/meeper/starbound-asset-converter)'s CompiledData output directory. 

## License

This application is released under an MIT style license.  In other words, it's released as is.  If you really need to rip this application off for your own application, you should probably rethink your career.

All Starbound game assets are, of course, property of Chucklefish.

## Project status

Well it exists.  I wrote this years ago while playing Starbound.  I'll occasionally update the game assets using the [Starbound Asset Converter](https://gitlab.com/meeper/starbound-asset-converter) utility but there's currently nothing further planned for this application.
